#!/usr/bin/env tclsh

namespace eval ::Private {
    variable check 0
    variable stepnumber 0
    variable checkpoint_failures 0
    variable ::Private::history [dict create steps [dict create]]

    # return the average of all the values in a passed-in list
    proc average {values} {
        set total 0.0
        foreach item $values {
            set total [expr $total + $item]
        }
        return [expr $total / [llength $values]]
    }

    proc die {message} {
        puts stderr "FATAL ERROR: $message"
        exit 1
    }

    proc readFileToString {fileName} {
        set file [open [lindex $fileName] "r"]

        set error [catch {
            seek $file 0 end
            set fileSize [tell $file]
            seek $file 0 start
            set output [read $file $fileSize]
        } errorDetail]

        if $error {
            close $file
            error $errorDetail
        }

        close $file
        return $output
    }

    proc getlastcommand {args} {
    }

    # Run a program and return the exit status, regardless of all other
    # circumstances
    proc runprogram {cmdline} {
        set error [catch {
            exec -- /bin/sh -c $cmdline >@stdout 2>@stderr
        } errorDetail]
        if $error {
            global errorCode
            if {$errorCode eq "NONE"} {
                return 0
            }
            puts $errorCode
            return [lindex $errorCode end]
        }
        return 0
    }

    proc finish_checkpoint {} {
        if {[info exists ::Private::checkpoint]} {
            set stepname [dict get $::Private::checkpoint name]
            if {[dict exists $::Private::history steps $stepname]} {
                set step_history [dict get $::Private::history steps $stepname]
            } else {
                set step_history {}
            }

            set history_average [::Private::average $step_history]

            set startTime [dict get $::Private::checkpoint startTime]
            set now [clock clicks -milliseconds]
            set time_diff [expr ( $now - $startTime ) / 1000.0]

            lappend step_history $time_diff

            while {[llength $step_history] > 7} {
                set step_history [lreplace $step_history 0 0]
            }

            dict set ::Private::history steps $stepname $step_history

            namespace eval ::Private::Checkpoint {
                variable number $::Private::stepnumber
                variable name [dict get $::Private::checkpoint name]
                variable run_time [uplevel 1 subst {$time_diff}]
                variable history_average [uplevel 1 subst {$history_average}]
                variable diff_from_average [expr {abs($history_average - $run_time)}]
                variable percent_diff_from_average [expr $diff_from_average / $history_average * 100.0]
                variable failures $::Private::checkpoint_failures
                variable status 0

                foreach threshold [dict get $::Private::checkpoint thresholds] {
                    if {![expr $threshold]} {
                        variable status 1
                    }
                }

                puts $::Private::outputfile [subst $::Private::timeformat]
            }

            set ::Private::checkpoint_failures 0
        }
    }

    proc main {} {
        if {$::argc < 1} {
            puts stderr "Must take a tcl shell file as an argument"
            exit 1
        }

        set script [readFileToString [lindex $::argv 0]]

        set error [catch {
            namespace eval ::Script $script
        } errorDetail]

        if $error {
            incr ::Private::checkpoint_failures
        }

        ::Private::finish_checkpoint

        if {[info exists ::Private::outputfile]} {
            close $::Private::outputfile
        }

        if {[info exists ::Private::historyfile]} {
            set file [open $::Private::historyfile w]
            puts $file $::Private::history
            close $file
        }
    }
}

namespace eval ::Script {
    proc check_status {status} {
        set ::Private::check $status
    }

    proc checkpoint_output_file {path} {
        set dir [file dirname $path]
        file mkdir $dir
        set ::Private::outputfile [open $path w]
    }

    proc checkpoint_history_file {path} {
        set ::Private::historyfile $path
        set dir [file dirname $path]
        file mkdir $dir
        if {[file exists $path]} {
            set ::Private::history [::Private::readFileToString $path]
        }
    }

    proc checkpoint {name args} {
        ::Private::finish_checkpoint
        set ::Private::checkpoint [dict create name $name thresholds $args startTime [clock clicks -milliseconds]]
        set ::Private::stepnumber [expr $::Private::stepnumber + 1]
    }

    proc checkpoint_time_format {format} {
        set ::Private::timeformat $format
    }

    proc checkpoint_status_format {format} {
        set ::Private::statusformat $format
    }

    proc statformat {format} {
        set ::Private::statformat $statformat
    }
}

proc unknown {args} {
    puts "check: $::Private::check"
    puts "executing: $args"

    set exitStatus [::Private::runprogram $args]

    if {$exitStatus != $::Private::check} {
        incr ::Private::checkpoint_failures
    }
}

::Private::main
