checkpoint_time_format {OS|AIX|Backup|[format %02d $number] $name|Time\t$run_time\t$status\tStep $name took [format %.2f $run_time] seconds.  The average runtime is [format %.2f $history_average] seconds.  This run was [format %.2f $percent_diff_from_average]% off.}
checkpoint_status_format {OS|AIX|Backup|[format %02d $number] $name|Status\t$failure_count\t$status\tStep $name exited with status $metric}

checkpoint_output_file /tmp/sample/checkpoint.log
checkpoint_history_file /tmp/sample/checkpoint.history

checkpoint Hello { $percent_diff_from_average < 10 }

sleep 1

echo test >/tmp/testfile

checkpoint There

sleep 2
